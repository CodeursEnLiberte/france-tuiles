import maplibregl from 'maplibre-gl';
import { Protocol } from 'pmtiles';


/**
 * Classe FranceTuiles
 * 
 * Cette classe permet d'afficher une carte de la France métropolitaine avec les DROMs à côté.
 * Il s’agit d’une surcouche à MaplibreGL.
 * 
 */
class FranceTuiles extends maplibregl.Map {
    constructor(containerId: string) {
        let protocol = new Protocol();
        maplibregl.addProtocol('pmtiles', protocol.tile);

        super({
            container: containerId,
            style: {
                version: 8,
                sources: {
                    'protomaps-source': {
                        'type': 'vector',
                        'url': 'pmtiles://http://france-tuiles.s3.fr-par.scw.cloud/metropole_droms.pmtiles',
                    }
                },
                layers: []
            },
            bounds: [-9.2771643, 41.3626917, 9.5600936, 51.0889894]
        });
    }

    /**
     * Affiche le contour des départements.
     * 
     * @param couleur_contours Couleur des contours des départements
     */
    afficherContoursDepartements(couleur_contours: string = '#198EC8') {
        this.on('load', () => {
            this.addLayer({
                'id': 'departements-contour',
                'source': 'protomaps-source',
                'source-layer': 'departements',
                'type': 'line',
                'paint': {
                    'line-color': couleur_contours,
                }
            });
        });
    }

    /**
     * Colorer certains départements, et optionnellement ajouter une couleur par défaut.
     * 
     * @param map_couleurs Couleurs des départements sous forme d'un objet {insee_dep: couleur}
     * @param couleur_defaut Couleur par défaut des départements
     */
    colorerDepartements(map_couleurs: {[insee_dep: string]: string} = {}, couleur_defaut: string = '#198EC8') {
        this.on('load', () => {
            this.addLayer({
                'id': 'colorer-departements',
                'source': 'protomaps-source',
                'source-layer': 'departements',
                'type': 'fill',
                'paint': {
                    'fill-color':
                        ['coalesce', ['get', ['get', 'insee_dep'], ['literal', map_couleurs]],
                            ['literal', couleur_defaut]]
                }
            },);
        });
    }
}

export {FranceTuiles};