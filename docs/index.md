# France Tuiles

Cette librairie sert à afficher une carte de France avec uniquement la métropole et les DROM visibles.

## Démarrage rapide

Copier / Coller cet exemple dans un fichier HTML

```html
<!DOCTYPE html>
<html>
  <head>
    <script src="https://unpkg.com/@codeureusesenliberte/france-tuiles@0.0.2/dist/france-tuiles.min.js"></script>
  </head>

  <body>
    <div style="width: 500px; height: 500px" id="map"></div>
  </body>
  <script defer>
    let map = new francetuiles.FranceTuiles("map");
    map.colorerDepartements(
      {
        63: "red",
        53: "yellow",
      },
      "blue"
    );
    map.afficherContoursDepartements();
  </script>
</html>
```

## Dans un projet JS

Installer `france-tuiles`

```bash
npm install npm i @codeureusesenliberte/france-tuiles
```

```js
let map = new francetuiles.FranceTuiles("map");
map.colorerDepartements(
  {
    63: "red",
    53: "yellow",
  },
  "blue"
);
map.afficherContoursDepartements();
```

![Carte de France avec deux départements colorés](images/index-1.png "Carte de France")
