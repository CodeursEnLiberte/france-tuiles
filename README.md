# France Tuiles

Cette librairie sert à afficher une carte de France avec uniquement la métropole et les DROM visibles.

Il s’agit d’une surcouche à [maplibregl](https://github.com/maplibre/maplibre-gl-js/) qui affiche une carte au format [PMTiles](https://docs.protomaps.com/pmtiles/) avec uniquement la France métropolitaine et les DROMs déplacés à côté de la France métropolitaine.

## Exemples

### NextJS

Ajouter la dépendence

`npm install npm i @codeureusesenliberte/france-tuiles`

Ajouter une page

```js
"use client";
import styles from "./page.module.css";
import React, { useEffect, useRef } from "react";

import { FranceTuiles } from "france-tuiles";

export default function Home() {
  const ref = useRef();

  useEffect(() => {
    const map = new FranceTuiles("map");
    map.colorerDepartements({
      63: "red",
      53: "yellow",
    });
    map.afficherContoursDepartements();
  }, []);

  return (
    <main className={styles.main}>
      <div className={styles.description}>
        <div style={{ width: "500px", height: "500px" }} id="map"></div>
      </div>
    </main>
  );
}
```

### HTML

```html
<!DOCTYPE html>
<html>
  <head>
    <script src="https://unpkg.com/@codeureusesenliberte/france-tuiles@0.0.2/dist/france-tuiles.min.js"></script>
  </head>

  <body>
    <div style="width: 500px; height: 500px" id="map"></div>
  </body>
  <script defer>
    let map = new francetuiles.FranceTuiles("map");
    map.colorerDepartements(
      {
        63: "red",
        53: "yellow",
      },
      "blue"
    );
    map.afficherContoursDepartements();
  </script>
</html>
```

## Développement

### Build

`npm run dist`
